
DROP DATABASE IF EXISTS cfernandez;
CREATE DATABASE IF NOT EXISTS cfernandez;
use cfernandez;

create table producto(
pro_id INT AUTO_INCREMENT PRIMARY KEY,
pro_nombre VARCHAR(30) NOT NULL,
pro_descripcion VARCHAR(50) NULL
);

create table precio(
pro_id int UNIQUE NOT NULL,
pre_precio decimal(5,2) NOT NULL,
pre_precio_oferta decimal(5,2),
pre_inicio_oferta date,
pre_fin_oferta date,
FOREIGN KEY (pro_id) REFERENCES producto(pro_id)
);



INSERT INTO producto(pro_nombre, pro_descripcion) VALUES ('papa', 'papa');
set @id=@@identity;
INSERT INTO precio(pro_id, pre_precio,pre_precio_oferta,pre_inicio_oferta, pre_fin_oferta)
 VALUES (@id, 1.00,0.80,'2021/05/01', '2021/05/05');

INSERT INTO producto(pro_nombre, pro_descripcion) VALUES ('manzana', 'manzana');
set @id=@@identity;
INSERT INTO precio(pro_id, pre_precio,pre_precio_oferta,pre_inicio_oferta, pre_fin_oferta)
 VALUES (@id, 0.25,0.20,'2021/04/15', '2021/04/30');

INSERT INTO producto(pro_nombre, pro_descripcion) VALUES ('coco', 'coco');
set @id=@@identity;
INSERT INTO precio(pro_id, pre_precio,pre_precio_oferta,pre_inicio_oferta, pre_fin_oferta)
 VALUES (@id, 0.50,0.40,'2021/05/01', '2021/05/15');


DELIMITER $$

CREATE PROCEDURE sp_obtener_producto(IN e_nombre VARCHAR(30))
BEGIN
	select pro_id,
	pro_nombre,
	pro_descripcion
	from  producto
	where pro_nombre = e_nombre;
END $$
DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_obtener_precio(IN e_id INT)
BEGIN
	select pre_precio,
	pre_precio_oferta,
	pre_inicio_oferta,
	pre_fin_oferta from precio
	where pro_id = e_id;
END $$
DELIMITER ;




