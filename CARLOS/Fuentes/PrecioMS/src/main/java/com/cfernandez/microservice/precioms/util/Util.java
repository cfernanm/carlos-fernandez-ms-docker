package com.cfernandez.microservice.precioms.util;

public class Util {

	public static final Integer codError = 9999;
	public static final String msjError = "Transaccion no exitosa";
	public static final Integer codExito = 0;
	public static final String msjExito = "Transaccion exitosa";

	public static void validaParametro(Object elemento, String campo) throws CustomException {
		if (elemento == null || (elemento instanceof String && String.valueOf(elemento).isEmpty())
				|| (elemento instanceof Integer && ((Integer) elemento) < 0))
			throw new CustomException(codError, msjError, "Se requiere " + campo);
	}
}
