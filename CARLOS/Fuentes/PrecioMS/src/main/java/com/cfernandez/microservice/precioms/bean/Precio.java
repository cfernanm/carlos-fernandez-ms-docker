package com.cfernandez.microservice.precioms.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Precio implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2342310902899312699L;
	private Integer idProducto;
	private BigDecimal precio;
	private BigDecimal precioOferta;
	private Date inicioOferta;
	private Date finOferta;
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	public BigDecimal getPrecioOferta() {
		return precioOferta;
	}
	public void setPrecioOferta(BigDecimal precioOferta) {
		this.precioOferta = precioOferta;
	}
	public Date getInicioOferta() {
		return inicioOferta;
	}
	public void setInicioOferta(Date inicioOferta) {
		this.inicioOferta = inicioOferta;
	}
	public Date getFinOferta() {
		return finOferta;
	}
	public void setFinOferta(Date finOferta) {
		this.finOferta = finOferta;
	}	
}
