package com.cfernandez.microservice.precioms.dao;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import com.cfernandez.microservice.precioms.bean.Precio;

@Repository
public class PrecioDAO {

	@Autowired
	StoredProcedureUtil storedProcedureUtil;


	@SuppressWarnings("unchecked")
	public Precio obtenerPrecio(Integer id) throws Exception {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("e_id", id);
		SqlParameter sqlParameters[] = { new SqlParameter("e_id", Types.VARCHAR) };
		Map<?, ?> execute = storedProcedureUtil.callStoredProcedureMysql("cfernandez", "sp_obtener_precio", parameters,
				sqlParameters);
		return obtenerPrecio((List<Map<String, Object>>) execute.get("#result-set-1"));
	}

	private Precio obtenerPrecio(List<Map<String, Object>> rs) {
		Precio prec = null;
		if (rs != null && rs.size() > 0) {
			prec = new Precio();
			Map<String, Object> rsProd = (Map<String, Object>) rs.get(0);
			prec.setPrecio((BigDecimal) rsProd.get("pre_precio"));
			prec.setPrecioOferta((BigDecimal) rsProd.get("pre_precio_oferta"));
			prec.setInicioOferta((Date) rsProd.get("pre_inicio_oferta"));
			prec.setFinOferta((Date) rsProd.get("pre_fin_oferta"));
		}
		return prec;
	}

}
