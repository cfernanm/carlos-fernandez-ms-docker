package com.cfernandez.microservice.precioms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cfernandez.microservice.precioms.bean.MensajeEntradaBuscarPrecio;
import com.cfernandez.microservice.precioms.bean.MensajeSalidaBuscarPrecio;
import com.cfernandez.microservice.precioms.bean.Precio;
import com.cfernandez.microservice.precioms.dao.PrecioDAO;
import com.cfernandez.microservice.precioms.util.CustomException;
import com.cfernandez.microservice.precioms.util.Util;

@Service
public class PrecioService {
	private final static Logger logger = LoggerFactory.getLogger(PrecioService.class);

	@Autowired
	PrecioDAO precioDao;

	public MensajeSalidaBuscarPrecio buscarPrecio(MensajeEntradaBuscarPrecio msjEntrada) {
		MensajeSalidaBuscarPrecio msjSalida = new MensajeSalidaBuscarPrecio();
		try {
			Util.validaParametro(msjEntrada.getId(), "id");
			Precio prec = precioDao.obtenerPrecio(msjEntrada.getId());

			if (prec != null) {
				prec.setIdProducto(msjEntrada.getId());
				msjSalida.setPrecio(prec);
				msjSalida.setCodigoError(Util.codExito);
				msjSalida.setMensajeUsuario(Util.msjExito);
				msjSalida.setMensajeSistema(Util.msjExito);
			} else {
				msjSalida.setCodigoError(Util.codError);
				msjSalida.setMensajeUsuario("Producto no existe");
				msjSalida.setMensajeSistema("Producto no existe");
			}
		} catch (CustomException e) {
			logger.error(e.getMensajeUsuario(), e);
			msjSalida.setCodigoError(e.getCodigoError());
			msjSalida.setMensajeUsuario(e.getMensajeUsuario());
			msjSalida.setMensajeSistema(e.getMessage());
		} catch (Exception e) {
			logger.error("Error al buscar Producto", e);
			msjSalida.setCodigoError(Util.codError);
			msjSalida.setMensajeUsuario(Util.msjError);
			msjSalida.setMensajeSistema(Util.msjError);
		}

		return msjSalida;
	}

}
