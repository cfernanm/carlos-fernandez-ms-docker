package com.cfernandez.microservice.precioms.dao;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

@Component
public class StoredProcedureUtil {

	@Autowired
	DataSourceManager dataSourceManager;

	public Map<?, ?> callStoredProcedureMysql(String catalogName, String sp, Map<String, Object> parameters,
			SqlParameter[] sqlParameters) {

		DataSource ds = dataSourceManager.mysqlDatasource();
		SimpleJdbcCall simpleJdbcCall = dataSourceManager.mysqlSimpleJdbcCall(ds);
		simpleJdbcCall.withCatalogName(catalogName);
		simpleJdbcCall.withProcedureName(sp);
		simpleJdbcCall.withoutProcedureColumnMetaDataAccess();
		simpleJdbcCall.declareParameters(sqlParameters);
		simpleJdbcCall.compile();
		MapSqlParameterSource inParams = new MapSqlParameterSource();
		if (parameters != null) {
			for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
				inParams.addValue(parameter.getKey(), parameter.getValue());
			}
		}
		return simpleJdbcCall.execute(inParams);
	}
}
