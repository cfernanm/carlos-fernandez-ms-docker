package com.cfernandez.microservice.precioms.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cfernandez.microservice.precioms.bean.MensajeEntradaBuscarPrecio;
import com.cfernandez.microservice.precioms.bean.MensajeSalida;
import com.cfernandez.microservice.precioms.bean.MensajeSalidaBuscarPrecio;
import com.cfernandez.microservice.precioms.service.PrecioService;
import com.cfernandez.microservice.precioms.util.Util;

@RestController
@RequestMapping(path = "/PrecioMS")
public class PrecioController {
	private final static Logger logger = LoggerFactory.getLogger(PrecioController.class);

	@Autowired
	PrecioService productoService;

	@RequestMapping(value = "/buscarPrecio", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<MensajeSalidaBuscarPrecio> buscarPrecio(HttpServletRequest request,
			@RequestBody MensajeEntradaBuscarPrecio msjEntrada) throws Exception {

		MensajeSalidaBuscarPrecio out = null;
		try {
			logger.info("Entrada metodo buscarPrecio");
			out = productoService.buscarPrecio(msjEntrada);
		} catch (Exception e) {
			logger.error("Eror al buscarPrecio", e);
			throw e;
		} finally {
			logger.info("Salida metodo buscarPrecio");
		}
		return new ResponseEntity<MensajeSalidaBuscarPrecio>(out, HttpStatus.OK);

	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<MensajeSalida> handleException(Exception exception) {
		MensajeSalida out = new MensajeSalida();
		out.setCodigoError(Util.codError);
		out.setMensajeUsuario(Util.msjError);
		out.setMensajeSistema(exception.getMessage() != null ? exception.getMessage() : Util.msjError);
		logger.error("Error general", exception);
		return new ResponseEntity<MensajeSalida>(out, HttpStatus.BAD_REQUEST);
	}

}
