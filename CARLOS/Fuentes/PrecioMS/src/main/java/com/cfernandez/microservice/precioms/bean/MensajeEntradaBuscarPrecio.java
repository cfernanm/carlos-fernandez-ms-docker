package com.cfernandez.microservice.precioms.bean;

import java.io.Serializable;

public class MensajeEntradaBuscarPrecio implements Serializable {
	private static final long serialVersionUID = -7008260407140281000L;
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
