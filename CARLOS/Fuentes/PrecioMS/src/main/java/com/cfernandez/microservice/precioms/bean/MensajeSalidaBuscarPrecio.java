package com.cfernandez.microservice.precioms.bean;

import java.io.Serializable;

public class MensajeSalidaBuscarPrecio extends MensajeSalida implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6314559853735511298L;
	Precio precio;

	public Precio getPrecio() {
		return precio;
	}

	public void setPrecio(Precio precio) {
		this.precio = precio;
	}
}
