package com.cfernandez.microservice.precioms.bean;

import java.io.Serializable;

public class MensajeSalida implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4597942650295587402L;
	private int codigoError;
	private String mensajeUsuario;
	private String mensajeSistema;
	public int getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}
	public String getMensajeUsuario() {
		return mensajeUsuario;
	}
	public void setMensajeUsuario(String mensajeUsuario) {
		this.mensajeUsuario = mensajeUsuario;
	}
	public String getMensajeSistema() {
		return mensajeSistema;
	}
	public void setMensajeSistema(String mensajeSistema) {
		this.mensajeSistema = mensajeSistema;
	}
}
