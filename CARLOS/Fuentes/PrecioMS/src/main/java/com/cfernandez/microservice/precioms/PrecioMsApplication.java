package com.cfernandez.microservice.precioms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrecioMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrecioMsApplication.class, args);
	}

}
