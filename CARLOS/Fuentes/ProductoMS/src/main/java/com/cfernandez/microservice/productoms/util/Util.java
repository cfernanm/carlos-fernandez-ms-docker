package com.cfernandez.microservice.productoms.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {
	public static final Integer codError = 9999;
	public static final String msjError = "Transaccion no exitosa";
	public static final Integer codExito = 0;
	public static final String msjExito = "Transaccion exitosa";
	

	public static void validaParametro(Object elemento, String campo) throws CustomException {
		if (elemento == null || (elemento instanceof String && String.valueOf(elemento).isEmpty()))
			throw new CustomException(codError, msjError, "Se requiere " + campo);
	}
	
	public static Date stringToDate(String inDate, String format) {
		try {
			Date date = null;
			SimpleDateFormat df = new SimpleDateFormat(format);
			date = df.parse(inDate);
			return date;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
