package com.cfernandez.microservice.productoms.util;

import java.io.Serializable;

public class CustomException extends Exception implements Serializable {
	/**
	 * 
	 */
	private Integer codigoError;
	private String mensajeUsuario;
	private static final long serialVersionUID = 5570830637989892776L;

	public CustomException(Integer codigoError, String mensajeUsuario, String mensajeSistema) {
		super(mensajeSistema);
		this.codigoError = codigoError;
		this.mensajeUsuario = mensajeUsuario;
	}

	public Integer getCodigoError() {
		return codigoError;
	}

	public String getMensajeUsuario() {
		return mensajeUsuario;
	}


}
