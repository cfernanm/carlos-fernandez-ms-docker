package com.cfernandez.microservice.productoms.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cfernandez.microservice.productoms.bean.MensajeEntradaBuscarProducto;
import com.cfernandez.microservice.productoms.bean.MensajeSalidaBuscarProducto;
import com.cfernandez.microservice.productoms.bean.Producto;
import com.cfernandez.microservice.productoms.dao.ProductoDAO;
import com.cfernandez.microservice.productoms.util.CustomException;
import com.cfernandez.microservice.productoms.util.Util;

@Service
public class ProductoService {
	private final static Logger logger = LoggerFactory.getLogger(ProductoService.class);

	@Value("${client.ws.precioms.buscarPrecio}")
	private String precioMsBuscarPrecio;

	@Autowired
	ProductoDAO productoDao;
	@Autowired
	WebServiceConfig config;
	

	public MensajeSalidaBuscarProducto buscarProducto(MensajeEntradaBuscarProducto msjEntrada) {
		MensajeSalidaBuscarProducto msjSalida = new MensajeSalidaBuscarProducto();
		try {
			Util.validaParametro(msjEntrada.getNombreProducto(), "nombreProducto");
			Util.validaParametro(msjEntrada.getFecha(), "fecha");
			// Map<?, ?> out = productoDao.obtenerProducto(msjEntrada.getNombreProducto(),
			// msjEntrada.getFecha());
			Producto prod = productoDao.obtenerProducto(msjEntrada.getNombreProducto(), msjEntrada.getFecha());
			if (prod != null) {

				prod.setPrecio(consultarPrecio(prod.getId(), msjEntrada.getFecha()));
				msjSalida.setProducto(prod);

				msjSalida.setCodigoError(Util.codExito);
				msjSalida.setMensajeUsuario(Util.msjExito);
				msjSalida.setMensajeSistema(Util.msjExito);

			} else {
				msjSalida.setCodigoError(Util.codError);
				msjSalida.setMensajeUsuario("Producto no existe");
				msjSalida.setMensajeSistema("Producto no existe");
			}
		} catch (CustomException e) {
			logger.error(e.getMensajeUsuario(), e);
			msjSalida.setCodigoError(e.getCodigoError());
			msjSalida.setMensajeUsuario(e.getMensajeUsuario());
			msjSalida.setMensajeSistema(e.getMessage());
		} catch (Exception e) {
			logger.error("Error al buscar Producto", e);
			msjSalida.setCodigoError(Util.codError);
			msjSalida.setMensajeUsuario(Util.msjError);
			msjSalida.setMensajeSistema(Util.msjError);
		}

		return msjSalida;
	}

	private BigDecimal consultarPrecio(Integer id, Date fecha) throws CustomException {
		BigDecimal precioRet = null;
		RestTemplate precioMS = null;
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("id", id);
			precioMS = config.clientePrecioMS();
			Map<String, Object> result = precioMS.postForObject(precioMsBuscarPrecio, parameters, Map.class);

			if (result != null) {
				if ((Integer) result.get("codigoError") == 0) {
					Map<String, Object> precio = (Map<String, Object>) result.get("precio");
					Date fInicio = Util.stringToDate((String) precio.get("inicioOferta"), "yyyy-MM-dd");
					Date fFin = Util.stringToDate((String) precio.get("finOferta"), "yyyy-MM-dd");
					if ((fecha.equals(fFin) || fecha.before(fFin)) && (fecha.equals(fInicio) || fecha.after(fInicio))) {
						precioRet = new BigDecimal((Double) precio.get("precioOferta"));
					} else {
						precioRet = new BigDecimal((Double) precio.get("precio"));
					}
				} else {
					throw new CustomException((Integer) result.get("codigoError"), (String) result.get("mensajeError"),
							(String) result.get("mensajeUsuario"));
				}
			}
		} catch (Exception e) {
			logger.error("Error al consultar precio", e);
			throw new CustomException(Util.codError, "Error al consultar precio de producto", e.getMessage());
		}
		if(precioRet!=null)
			precioRet=precioRet.setScale(2, BigDecimal.ROUND_DOWN);
		return precioRet;
	}

}
