package com.cfernandez.microservice.productoms.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Configuration
public class DataSourceManager {

	
	@Bean("mysqlDatasource")
	@ConfigurationProperties("spring.mysql.datasource")
	public DataSource mysqlDatasource() {
		return DataSourceBuilder.create().build();
	}

//	public JdbcTemplate mysqlJdbcTemplate(@Qualifier("mysqlDatasource") DataSource mysqlDatasource) {
//		return new JdbcTemplate(mysqlDatasource);
//	}

	public SimpleJdbcCall mysqlSimpleJdbcCall(@Qualifier("mysqlDatasource") DataSource mysqlDatasource) {
		return new SimpleJdbcCall(mysqlDatasource);
	}

}
