package com.cfernandez.microservice.productoms.bean;

import java.io.Serializable;

public class MensajeSalidaBuscarProducto extends MensajeSalida implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6314559853735511298L;
	Producto producto;

	
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	

}
