package com.cfernandez.microservice.productoms.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class Producto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2342310902899312699L;
	private Integer id;
	private String nombre;
	private String descripcion;
	private BigDecimal precio;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}	
}
