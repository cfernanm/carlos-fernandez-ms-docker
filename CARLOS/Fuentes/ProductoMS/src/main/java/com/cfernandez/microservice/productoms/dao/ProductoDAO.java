package com.cfernandez.microservice.productoms.dao;

import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import com.cfernandez.microservice.productoms.bean.Producto;

@Repository
public class ProductoDAO {

	@Autowired
	StoredProcedureUtil storedProcedureUtil;
//
//	public Map<?, ?> obtenerProducto(String nombreProducto, Date fecha) throws Exception {
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("e_nombre", nombreProducto.toLowerCase());
//		SqlParameter sqlParameters[] = { new SqlParameter("e_nombre", Types.VARCHAR),
//				new SqlReturnResultSet("rs1", new GenericRowMapper()) };
//		return storedProcedureUtil.callStoredProcedureMysql("cfernandez", "sp_obtener_producto", parameters,
//				sqlParameters);
//
//	}

	@SuppressWarnings("unchecked")
	public Producto obtenerProducto(String nombreProducto, Date fecha) throws Exception {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("e_nombre", nombreProducto.toLowerCase());
		SqlParameter sqlParameters[] = { new SqlParameter("e_nombre", Types.VARCHAR) };
		Map<?, ?> execute = storedProcedureUtil.callStoredProcedureMysql("cfernandez", "sp_obtener_producto",
				parameters, sqlParameters);
		return obtenerProducto((List<Map<String, Object>>) execute.get("#result-set-1"));
	}

	private Producto obtenerProducto(List<Map<String, Object>> rs) {
		Producto prod = null;
		if (rs != null && rs.size() > 0) {
			prod = new Producto();
			Map<String, Object> rsProd = (Map<String, Object>) rs.get(0);
			prod.setId((Integer) rsProd.get("pro_id"));
			prod.setDescripcion((String) rsProd.get("pro_descripcion"));
			prod.setNombre((String) rsProd.get("pro_nombre"));
		}
		return prod;
	}

}
