package com.cfernandez.microservice.productoms.service;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

@Service
public class WebServiceConfig {
	
	@Value("${client.ws.precioms.url}")
    private String precioMSUrl;
	@Value("${client.ws.precioms.conntimeout}")
	private int precioMSConnTimeout;
	@Value("${client.ws.precioms.readtimeout}")
	private int precioMSReadTimeout;

	private RestTemplate clientePrecioMS = null;

	public RestTemplate clientePrecioMS() throws Exception {
		if (clientePrecioMS == null) {
			clientePrecioMS = new RestTemplate();
			DefaultUriTemplateHandler handler = new DefaultUriTemplateHandler();
			handler.setBaseUrl(precioMSUrl);
			clientePrecioMS.setUriTemplateHandler(handler);
			setTimeouts(clientePrecioMS, precioMSConnTimeout, precioMSReadTimeout);
		}
		return clientePrecioMS;
	}

	private void setTimeouts(RestTemplate restTemplate, int connTimeout, int readTimeout) {
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(readTimeout)
				.setConnectTimeout(connTimeout).setSocketTimeout(readTimeout).build();
		CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setHttpClient(httpClient);
		restTemplate.setRequestFactory(clientHttpRequestFactory);
	}

}
