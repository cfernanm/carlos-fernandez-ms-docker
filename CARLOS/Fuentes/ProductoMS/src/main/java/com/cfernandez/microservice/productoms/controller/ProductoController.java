package com.cfernandez.microservice.productoms.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cfernandez.microservice.productoms.bean.MensajeEntradaBuscarProducto;
import com.cfernandez.microservice.productoms.bean.MensajeSalida;
import com.cfernandez.microservice.productoms.bean.MensajeSalidaBuscarProducto;
import com.cfernandez.microservice.productoms.service.ProductoService;
import com.cfernandez.microservice.productoms.util.Util;

@RestController
@RequestMapping(path = "/ProductoMS")
public class ProductoController {
	@Autowired
	ProductoService productoService;
	private final static Logger logger = LoggerFactory.getLogger(ProductoController.class);

	@RequestMapping(value = "/buscarProducto", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<MensajeSalidaBuscarProducto> buscarProducto(HttpServletRequest request,
			@RequestBody MensajeEntradaBuscarProducto msjEntrada) throws Exception {
		MensajeSalidaBuscarProducto out = null;
		try {
			logger.info("Entrada metodo buscarProducto");
			out = productoService.buscarProducto(msjEntrada);
		} catch (Exception e) {
			logger.error("Eror al buscarProducto", e);
			throw e;
		} finally {
			logger.info("Salida metodo buscarProducto");
		}
		return new ResponseEntity<MensajeSalidaBuscarProducto>(out, HttpStatus.OK);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<MensajeSalida> handleException(Exception exception) {
		MensajeSalida out = new MensajeSalida();
		out.setCodigoError(Util.codError);
		out.setMensajeUsuario(Util.msjError);
		out.setMensajeSistema(exception.getMessage() != null ? exception.getMessage() : Util.msjError);
		logger.error("Error general", exception);
		return new ResponseEntity<MensajeSalida>(out, HttpStatus.BAD_REQUEST);
	}

}
